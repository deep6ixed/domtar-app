
//Custom Functions for application

//TroubleLogData Object and built in functions - keeps everything in one place

var TroubleLogData = {
	faulttime: ["00:00"],
	machine: ["None"],
	fault: ["No Logs Recorded"],
	duration: ["00"]
}

var BookmarkData = {
	title: ["None"],
	url: ["None"]
}

var PartData = {
	partid: [],
	partdesc: [],
	partloc: []
}

var SettingsData = {
	exportemail: ["none@noemail.com"],
	clockedin: false
}

var FCM = {
	type: "None",
	token: "None"
}

var PushNotificationData = {
	title: ["None"],
	message: ["No Notifications"]
}

var push = [];


//Handles all functions for the troublelog
TroubleLog = {
	store: function() {
		var DataToStore = JSON.stringify(TroubleLogData);
		window.localStorage.setItem("TroubleLogData", DataToStore);
	},

	blank: function(initialize) {
		window.localStorage.removeItem("TroubleLogData");
		TroubleLogData.machine.length = 0;
		TroubleLogData.fault.length = 0;
		TroubleLogData.faulttime.length = 0;
		TroubleLogData.duration.length = 0;
		if (initialize == 1) {
			TroubleLogData.faulttime[0] = "00:00";
			TroubleLogData.machine[0] = "None";
			TroubleLogData.fault[0] = "No Logs Recorded";
			TroubleLogData.duration[0] = "00";
		};

		TroubleLog.display("TroubleLog");
	},

	remove: function(lognumber) {
		if (TroubleLogData.machine.length == 1) {
			TroubleLogData.machine[0] = "None";
			TroubleLogData.fault[0] = "No Logs Recorded";
			TroubleLogData.faulttime[0] = "00:00";
			TroubleLogData.duration[0] = "00";
		} else {
			TroubleLogData.machine.splice(lognumber, 1);
			TroubleLogData.fault.splice(lognumber, 1);
			TroubleLogData.faulttime.splice(lognumber, 1);
			TroubleLogData.duration.splice(lognumber, 1);
		};

		TroubleLog.store();
		TroubleLog.display("TroubleLog");
		$.tab('change tab', 'fifth');
		TroubleLog.formclear();
	},

	editentry: function(lognumber) {
		$("#LogEnter").addClass("disabled");
		$("#LogUpdate").removeClass("disabled");
		$("#LogDelete").removeClass("disabled");

		//$('#machine').val(TroubleLogData.machine[lognumber]);
		$(".ui.dropdown.machine").dropdown('set selected', TroubleLogData.machine[lognumber]);
		$("#fault").val(TroubleLogData.fault[lognumber]);
		$("#faulttimecode").val(TroubleLogData.faulttime[lognumber]);
		$("#faultduration").val(TroubleLogData.duration[lognumber]);
		$("#logID").val(lognumber);

		$.tab('change tab', 'fourth');

	},

	updateentry: function() {

		var lognumber = $("#logID").val();

		TroubleLogData.machine[lognumber] = $("#machine").val();
		TroubleLogData.fault[lognumber] = $("#fault").val();
		TroubleLogData.faulttime[lognumber] = $("#faulttimecode").val();
		TroubleLogData.duration[lognumber] = $("#faultduration").val();

		TroubleLog.store();
		TroubleLog.display("TroubleLog");
		$.tab('change tab', 'fifth');
		TroubleLog.formclear();
	},


	addentry: function() {

		if (TroubleLogData.machine[0] == "None") {
			TroubleLogData.machine[0] = $("#machine").val();
			TroubleLogData.fault[0] = $("#fault").val();
			TroubleLogData.faulttime[0] = $("#faulttimecode").val();
			TroubleLogData.duration[0] = $("#faultduration").val();
		} else {
			TroubleLogData.machine.push($("#machine").val());
			TroubleLogData.fault.push($("#fault").val());
			TroubleLogData.faulttime.push($("#faulttimecode").val());
			TroubleLogData.duration.push($("#faultduration").val());
		};
		TroubleLog.store();
		TroubleLog.display("TroubleLog");
		TroubleLog.formclear();
	},

	display: function(tableID) {
		var fulltablebodyid = "#" + tableID + " > tbody";
		$(fulltablebodyid).empty();

		var index;
		for (index = 0; index < TroubleLogData.machine.length; ++index) {

			//breakdown the time into two rows to make it smaller on the table display:
			var hourstart = TroubleLogData.faulttime[index].length - 5;
			var hourstop = hourstart + 5;
			var faulttime = TroubleLogData.faulttime[index].slice(hourstart, hourstop);
			var faultdate = TroubleLogData.faulttime[index].slice(0, hourstart);

			var row = "<tr><td class='collapsing'><div class='content' id='bold'>" + faultdate + "</div><div class='sub header'>" + faulttime + "</div></td><td><div class='content' id='bold'>" + TroubleLogData.machine[index] + "</div><div class='sub header'>" + TroubleLogData.fault[index] + "</div></td><td class='collapsing'>" + TroubleLogData.duration[index] + "</td>";

			if (TroubleLogData.machine[index] == "None") {
				var rowend = "<td class='collapsing'><button class='ui icon button disabled'><i class='trash alternate icon'></i></button></td></tr>";
				var newRow = row + rowend;
			} else {
				var rowend = "<td class='collapsing'><button class='ui icon button' onClick='TroubleLog.editentry(" + index + ")'><i class='pencil alternate icon small'></i></button></td></tr>";
				var newRow = row + rowend;
			};

			$(fulltablebodyid).append(newRow);
		}

		$("#LogEnter").removeClass("disabled");
		$("#LogUpdate").addClass("disabled");
		$("#LogDelete").addClass("disabled");
	},

	formclear: function() {
		$("#fault").val("");
		$("#faulttimecode").val("");
		$("#faultduration").val("");
		$("#logID").val();
	},

	displayhidden: function(tableID) {
		var fulltablebodyid = "#" + tableID + " > tbody";
		$(fulltablebodyid).empty();

		var index;
		for (index = 0; index < TroubleLogData.machine.length; ++index) {

			var row = "<tr><td>" + TroubleLogData.faulttime[index] + "</td><td>" + TroubleLogData.machine[index] + "</td><td>" + TroubleLogData.fault[index] + "</td><td>" + TroubleLogData.duration[index] + "</td></tr>";

			$(fulltablebodyid).append(row);
		}
	},

	load: function() {
		var retrievedTroubleLog = window.localStorage.getItem("TroubleLogData");

		if (retrievedTroubleLog == null) {} else {
			TroubleLogData = JSON.parse(retrievedTroubleLog);
		}

		TroubleLog.display("TroubleLog");
	},

	externalexport: function(tableID) {

		TroubleLog.displayhidden("TroubleLogHidden");

		var doc = new jsPDF()
		doc.text(85, 10, "Trouble Log");

		// It can parse html:
		doc.autoTable({
			html: '#TroubleLogHidden',
			columnStyles: {
				0: {
					cellWidth: 40
				},
				1: {
					cellWidth: 20
				},
				2: {
					cellWidth: 95
				},
				3: {
					cellWidth: 20
				}
			}
		})


		var out = doc.output('blob');
		var reader = new FileReader();
		var base64data = "";

		reader.readAsDataURL(out);
		reader.onloadend = function() { // for blob to base64
			base64data = reader.result;
			rawbase64 = base64data.replace('data:application/pdf;base64,', '');
			//	console.log("base64 data is ");               
			//	console.log(rawbase64);
		}
		//doc.save('log.pdf');
		cordova.plugins.email.open({
			to: SettingsData.exportemail,
			subject: 'Export Trouble Log',
			body: 'Automatic email generated by Domtar Application.',
			attachments: ['base64:Troublelog.pdf//' + rawbase64]
		});
	}

};

//Handles all the functions for the Bookmarking features
Bookmark = {
	store: function() {
		var DataToStore = JSON.stringify(BookmarkData);
		window.localStorage.setItem("BookmarkData", DataToStore);
	},

	blank: function(initialize) {
		window.localStorage.removeItem("BookmarkData");
		BookmarkData.title.length = 0;
		BookmarkData.url.length = 0;
		if (initialize == 1) {
			BookmarkData.title[0] = "None";
			BookmarkData.url[0] = "None";
		};

		Bookmark.display("Bookmark");
	},

	remove: function(bookmarknumber) {
		if (BookmarkData.title.length == 1) {
			BookmarkData.title[0] = "None";
			BookmarkData.url[0] = "None";
		} else {
			BookmarkData.title.splice(bookmarknumber, 1);
			BookmarkData.url.splice(bookmarknumber, 1);
		};
		Bookmark.store();
		Bookmark.display("Bookmark");
	},

	addentry: function() {
		var BookmarkTitle = document.getElementById("wikiframe").contentDocument.title;
		var BookmarkURL = document.getElementById("wikiframe").contentWindow.location.href;

		$('#submitbookmark').toast({
			showProgress: 'bottom',
			classProgress: 'green',
			position: 'top center',
			displayTime: 1500,
		});

		if (BookmarkData.title[0] == "None") {
			BookmarkData.title[0] = BookmarkTitle;
			BookmarkData.url[0] = BookmarkURL;
		} else {
			BookmarkData.title.push(BookmarkTitle);
			BookmarkData.url.push(BookmarkURL);
		};
		Bookmark.store();
		Bookmark.display("Bookmark");
	},

	display: function(tableID) {
		var fulltablebodyid = "#" + tableID + " > tbody";
		$(fulltablebodyid).empty();

		var index;
		for (index = 0; index < BookmarkData.title.length; ++index) {

			if (BookmarkData.title[index] == "None") {
				var newRow = "<tr><td>None</td><td><button class='ui icon button disabled'><i class='trash alternate icon'></i></button></td></tr>";
			} else {
				var newRow = "<tr><td><a href='" + BookmarkData.url[index] + "' target='wikiframe' onclick='GoWikiTab()'>" + BookmarkData.title[index] + "</a></td><td class='collapsing'><button class='ui icon button' ondblClick='Bookmark.remove(" + index + ")'><i class='trash alternate icon'></i></button></td></tr>";
			};
			$(fulltablebodyid).append(newRow);
		}
	},

	load: function() {
		var retrievedBookmark = window.localStorage.getItem("BookmarkData");

		if (retrievedBookmark == null) {} else {
			BookmarkData = JSON.parse(retrievedBookmark);
		}

		Bookmark.display("Bookmark");
	}

};

//Handles all the functions for the parts table.
Parts = {

	filter: function(SortTable, SortInput) {
		var input, filter, table, tr, td, i;
		input = document.getElementById(SortInput);
		
	
		if (input.value ==""){
			return;
		} else {
		
			filter = input.value.toUpperCase();
			table = document.getElementById(SortTable);
			tr = table.getElementsByTagName("tr");
			for (var i = 0; i < tr.length; i++) {
				var tds = tr[i].getElementsByTagName("td");
				var flag = false;
				for (var j = 0; j < tds.length; j++) {
					var td = tds[j];
					if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
						flag = true;
					}
				}
				if (flag) {
					tr[i].style.display = "table-row";
					tr[i].style.border = "1";
				} else {
					tr[i].style.display = "none";
				}
			
		}
		}

		Parts.detail(SortTable);

	},

	detail: function(SortTable) {
		var table = document.getElementById('SortTable');
		var cells = table.getElementsByTagName('td');

		for (var i = 0; i < cells.length; i++) {
			var cell = cells[i];
			cell.onclick = function() {
				var rowId = this.parentNode.rowIndex;
				var rowSelected = table.getElementsByTagName('tr')[rowId];

				$("#DetailModalPartNumber").html(rowSelected.cells[0].innerHTML);
				$("#DetailModalPartDesc").html(rowSelected.cells[1].innerHTML);
				$("#DetailModalPartLoc").html(rowSelected.cells[2].innerHTML);

				$('.ui.modal#PartsDetail').modal('show');
			}
		}
	}
}

//Handles all functions for Settings Storage and retreiving.
Settings = {
	load: function() {
		var retrievedSettings = window.localStorage.getItem("SettingsData");

		if (retrievedSettings == null) {} else {
			SettingsData = JSON.parse(retrievedSettings);
		}
		$('input[name=exportemail]').val(SettingsData.exportemail);
	},

	store: function() {
		var DataToStore = JSON.stringify(SettingsData);
		window.localStorage.setItem("SettingsData", DataToStore);
	},

	update: function() {
		SettingsData.exportemail = $('[name=exportemail]').val();
		$('input[name=exportemail]').val(SettingsData.exportemail);
		Settings.store();
	}
}

//Handles Local Notification Code.
LocalNotifications = {
	test: function(){
		cordova.plugins.notification.local.schedule({
			id: 1,
			title: 'Test Notification',
			text: 'It Works...',
			trigger: {
			in: 1,
				unit: 'minute'
			},
			vibrate: true
		});
	}
}

//Handles all the specific actions based on what the actual push message type is
PushHandler = {
	update: function(data){
		//data format:
		//No additional data pushed with this message
		$('.ui.basic.modal#UpdateFCM').modal('show');
		PushHandler.add("Update",data.title);
	},
	
	breakdown: function(data){
		//data format:
		//data.additionalData.data2 = Line that has an issue
		//data.additionalData.data3 = Specific Fault
		$("#FCMLine").html(data.additionalData.data2);
		$("#FCMFault").html(data.additionalData.data3);
		$('.ui.basic.modal#BreakdownFCM').modal('show');
		PushHandler.add("Breakdown: "+data.additionalData.data2, data.additionalData.data3)
	},
	
	revoke: function(){
		//data format:
		//No additional data pushed with this message
		push.unsubscribe('updates', function () {
			console.log('topic unsub success');
		}, function (e) {
			console.log('topic unsub error:', e);
		});
		push.unsubscribe('breakdown', function () {
			console.log('topic unsub success');
		}, function (e) {
			console.log('topic unsub error:', e);
		});
	},
	
	remove: function(messageID){
		if (PushNotificationData.title.length == 1) {
			PushNotificationData.title[0] = "None";
			PushNotificationData.message[0] = "No Notifications";
		} else {
			PushNotificationData.title.splice(messageID, 1);
			PushNotificationData.message.splice(messageID, 1);
		};
		PushHandler.store();
		PushHandler.display();
	},
	
	add: function(title, message){
		if (PushNotificationData.title[0] == "None") {
			PushNotificationData.title[0] = title;
			PushNotificationData.message[0] = message;
		} else {
			PushNotificationData.title.push(title);
			PushNotificationData.message.push(message);
		};
		PushHandler.store();
		PushHandler.display();
	},
	
	display: function(){
		$("#NotificationArea").empty();
		console.log(PushNotificationData.title.length);
		var index;
		for (index = 0; index < PushNotificationData.title.length; ++index) {
			$("#NotificationArea").prepend("<div class='ui message' onclick='PushHandler.remove("+index+")'><div class='header'>"+PushNotificationData.title[index]+"</div><p>"+PushNotificationData.message[index]+"</p></div>");
		}
		
	},
	
	load: function(){
		var retrievedPushNotificationData = window.localStorage.getItem("PushNotificationData");

		if (retrievedPushNotificationData == null) {} else {
			PushNotificationData = JSON.parse(retrievedPushNotificationData);
		}
		PushHandler.display();
	},
	
	store: function() {
		var DataToStore = JSON.stringify(PushNotificationData);
		window.localStorage.setItem("PushNotificationData", DataToStore);
		PushHandler.display();
	},
	
}	

//Handles the Punch in and out to receive messages		
Clock = {
	
	load: function(){
		console.log(SettingsData.clockedin);
		if(SettingsData.clockedin == true) {
			console.log("clocked in");
			Clock.punchin();
		}
	},
	
	punchin: function(){
		console.log("clocked in");
		SettingsData.clockedin = true;
		$("#ClockStatus").html("<i class='clock icon'></i>Clocked In");
		$("#ClockMessage").html("<p>Currently Clocked In and receiving Breakdown Notifications</p>");
		$("#ClockDiv").addClass("blue");
		$("#ClockDiv").removeClass("grey");	
		Settings.store();
		
		push.subscribe('breakdown', function () {
			console.log('topic sub success');
		}, function (e) {
			console.log('topic sub error:', e);
		});
		
		
	},
	
	punchout: function(){
		SettingsData.clockedin = false;
		$("#ClockStatus").html("<i class='clock outline icon'></i>Clocked Out");
		$("#ClockMessage").html("<p>Currently Clocked Out and not receiving Breakdown Notifications</p>");
		$("#ClockDiv").addClass("grey");
		$("#ClockDiv").removeClass("blue");
		Settings.store();
		
		push.unsubscribe('breakdown', function () {
			console.log('topic unsub success');
		}, function (e) {
			console.log('topic unsub error:', e);
		});
		
		
	}
	
}	

//Handles Push Notification Code
function PushNotifications(){
	push = PushNotification.init({
		android: {
				senderID: '207235412584'
		},
		browser: {
			pushServiceURL: 'http://push.api.phonegap.com/v1/push'
		},
		ios: {
			alert: "true",
			badge: "true",
			sound: "true"
		},
		windows: {}
	});

	push.on('registration', function (data) {
		FCM.token = data.registrationId;
		FCM.type = data.registrationType;
		$("#PushStatus").html("Registered: " + FCM.type);
		$("#PushToken").html(FCM.token);
	});
	
	push.on('error', function (e) {
		alert("error:" + e);
	});

	push.on('notification', function (data) {
		//Data Fomat:
		//data.additionalData.data1 = Notification Type
		
		if (data.additionalData.data1 == "breakdown"){
			PushHandler.breakdown(data);
		} else if (data.additionalData.data1 == "updates"){
			PushHandler.update(data);
		} else if (data.additionalData.data1 == "revoke"){
			PushHandler.revoke();
		} else {
			alert("Unknown FCM message received! Type identified as: " + data.additionalData.data1);
		}
		
	});
}	


//Handles Application and Cordova Code
App = {
	init: function(){
		TroubleLog.load();
		Bookmark.load();
		Settings.load();
		PushHandler.load();
		Clock.load();
		$('table').tablesort();
	}
	
}

//Override Functions
$("#logForm").submit(function(event) {
	event.preventDefault();
});

//Stops enter key from doing shit when there is no enter key on a mobile deviceready
$(document).ready(function() {
	$(window).keydown(function(event) {
		if (event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
});


//Handles Navigation Function
$('.menu .item')
	.tab();

$('.menu .item')
	.on('click', function() {
		$('.ui.sidebar').sidebar('hide');
	});

//Calender Controls
$('#standard_calendar')
	.calendar({
		ampm: false
	});

//Handles Drop downs in forms
$('.selection.dropdown')
	.dropdown();

//Closes Popup Messages
$('.message .close')
	.on('click', function() {
		$(this)
			.closest('.message')
			.transition('fade');
	});

//UI Button Controllers - OnClick Functions
$("#PartDetails").click(function() {
	$('.ui.sidebar').sidebar('toggle');
});

$("#LogExport").click(function() {
	TroubleLog.externalexport("#TroubleLog");
});

$("#MainMenu").click(function() {
	$('.ui.sidebar').sidebar('toggle');
});

$("#LogRefresh").click(function() {
	TroubleLog.display("TroubleLog");
});

$("#BookmarkRefresh").click(function() {
	Bookmark.display("Bookmark");
});

$("#SettingsRefresh").click(function() {
	event.preventDefault();
	Settings.update();
});

$("#LogReset").click(function() {
	$('.ui.basic.modal#FlushLog')
		.modal({
			closable: false,
			onDeny: function() {},
			onApprove: function() {
				TroubleLog.blank(1);
				TroubleLog.display();
			}
		})
		.modal('show');
});

$("#PushNotifications").click(function() {
	event.preventDefault();
	$('.ui.basic.modal#FCMStatus').modal('show');
});

$("#SubUpdates").click(function () {
	event.preventDefault();
	push.subscribe('breakdown', function () {
		console.log('topic sub success');
	}, function (e) {
		console.log('topic sub error:', e);
	});
});

$("#UnsubUpdates").click(function () {
	event.preventDefault();
	push.unsubscribe('updates', function () {
		console.log('topic unsub success');
	}, function (e) {
		console.log('topic unsub error:', e);
	});
		
});

$("#BookmarkReset").click(function() {
	$('.ui.basic.modal#FlushBookmarks')
		.modal({
			closable: false,
			onDeny: function() {},
			onApprove: function() {
				Bookmark.blank(1);
				Bookmark.display();
			}
		})
		.modal('show');
});

$("#LogUpdate").click(function() {
	TroubleLog.updateentry();
});

$("#LogDelete").click(function() {
	TroubleLog.remove($("#logID").val());
});

$("#LogEnter").click(function() {
	if ($('.ui.form.logentry').form('is valid')) {
		TroubleLog.addentry();

		$('#submitlog').toast({
			showProgress: 'bottom',
			classProgress: 'green',
			position: 'top center',
			displayTime: 1500
		});

		TroubleLog.display("TroubleLog");
	} else {
		$('#logerror').toast({
			showProgress: 'bottom',
			classProgress: 'red',
			position: 'top center',
			displayTime: 2000
		});
	}
});

$('#TestNotification')
	.on('click', function() {
		event.preventDefault();
		LocalNotifications.test();
});

$( "#ClockPress" ).click(function() {
	if (SettingsData.clockedin == false){
		Clock.punchin();
	} else {
		Clock.punchout();
	}
});


//Switches page to Wiki Tab

function GoWikiTab() {
	$.tab('change tab', 'second');
}


// Form Validation Code




function onBackButton() {
	wikiframe.history.back();
}

function onWindowLoad() {
	
}

function onDeviceReady(){
	PushNotifications();
}

//Document and Window Event Handler Code


document.addEventListener("backbutton", onBackButton, false);
window.addEventListener("load", onWindowLoad, false);
document.addEventListener('DOMContentLoaded', App.init, false);
document.addEventListener('deviceready', onDeviceReady, false);



